<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    public function Information()
    {
        return $this->hasMany('App\ActivityInformation');
    }
}
