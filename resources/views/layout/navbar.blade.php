<nav class="shadow navbar py-3 navbar-expand-md navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
        <img src="{{asset('img/logo.png')}}" alt="logo" class="logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04"
            aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsExample04">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown px-3">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown02" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">Lieux favoris</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown02">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item dropdown px-3">
                    <a class="nav-link dropdown-toggle mr-3" href="#" id="dropdown03" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">Guide touristique</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown03">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
            
                <li class="nav-item">
                    @if (Route::has('login'))
                        @auth
                            <a href="{{ url('/') }}">Accueil</a>
                        @else
                            @if (Route::has('register'))
                                 <a class="btn btn-primary text-white nav-link" href="{{ route('register') }}">Inscription / Connexion</a>
                            @endif
                        @endauth
                @endif
                </li>
            </ul>
        </div>
    </div>
</nav>
