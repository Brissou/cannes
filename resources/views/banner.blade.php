<div id="banner" class="d-flex align-items-center position-relative" style="background-image:url('{{asset('img/bg.jpg')}}')">
    <div class="container">
        <div>
            <h1>Découvrez Cannes</h1>
            <h3>Explorez et visitez les endroits les plus incontournable de cette ville</h3>
            <div class="d-flex">

            </div>
        </div>
    </div>
    <img src="{{asset('img/sep-white.png')}}" class="separator w-100" alt="separator">
</div>