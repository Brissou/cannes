<div id="popular" class="pb-5 bg-white">
    <div class="container">
        <div class="title text-center py-4">
            <div class="d-flex justify-content-center align-items-center">
                <div class="line"></div>
                    <h5 class="m-0 px-3">Les immanquables de notre belle ville</h5>
                <div class="line"></div>
            </div>
            <h2 class="my-2">Lieux Favoris</h2>
        </div>
        <div class="row">
            @foreach ($activities as $activity)
                <div class=" p-3 {{ $loop->last || $loop->first ? 'col-lg-6' : 'col-lg-3' }}">
                <a href="{{url($activity->slug)}}" class="text-decoration-none position-relative text-white cardo rounded d-flex flex-column justify-content-between p-3">
                        <div class="filter"></div>
                        <img src="{{ asset('img/'.$activity->slug.'/1.jpg') }}" class="rounded" alt="bg">
                        <div class="number rounded px-3 align-self-end">20 Résultats</div>
                        <div class="name text-center">
                            <h3>{{$activity->title}}</h3>
                            <h5>{{$activity->subtitle}}</h5>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>