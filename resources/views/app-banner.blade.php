<div id="app-banner">
    <div class="container h-100 position-relative overflow-hidden">
    <img src="{{asset('img/app.png')}}" class="app-img d-none d-lg-block" alt="app">
        <div class="col-lg-6 h-100 d-flex align-items-center">
            <div class="d-flex text-white flex-column justify-content-center">
                <h2>Téléchargez l'app de Cannes</h2>
                <p>Avec cette application vous découvrirez les plus beaux endroits et les meilleurs coins de notre belle ville.</p>
                <div class="d-flex">
                    <div class="btn btn-primary text-white d-flex align-items-center justify-content-between py-1 px-4">
                        <i class="fas fa-download"></i>
                        <div class="text-right ml-3">
                            <span class="text-uppercase">Télécharger sur</span>
                            <h5 class="mb-1">APP STORE</h5>
                        </div>
                    </div>
                    <div class="btn btn-primary text-white d-flex align-items-center justify-content-between ml-3 py-1 px-4">
                        <i class="fas fa-download"></i>
                        <div class="text-right ml-3">
                            <span class="text-uppercase">Télécharger sur</span>
                            <h5 class="mb-1">PLAY STORE</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
