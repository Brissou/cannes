<div id="guides" class="py-5 my-3">
<div class="title text-center py-4">
    <div class="d-flex justify-content-center align-items-center">
        <div class="line"></div>
            <h5 class="m-0 px-3">Quelques idées de voyages</h5>
        <div class="line"></div>
    </div>
    <h2 class="my-2">Guide Touristique</h2>
</div>

    <div class="container">
        <div class="row">
            @foreach ($guides as $guide)
                <div class="col-md-4 p-3">
                    <a href="{{url('guides/'.$guide->img)}}" class="d-flex flex-column decoration-none">
                        <img src="{{asset('img/guides/'.$guide->img.'.jpg')}}" class="shadow-sm rounded" alt="">
                        <div class="py-3">
                            <span class="text-danger">{{$guide->theme}}</span>
                            <span>|</span>
                            <span>{{$guide->author}}</span>
                        </div>
                        <h3>{{$guide->title}}</h3>
                        <p>{{$guide->subtitle}}</p>
                    </a>
                    </div>
            @endforeach
        </div>
    </div>
</div>