@extends('layout.app')
@section('content')

<div id="header-title" style="background-image:url({{$images[0]}})">
        <div class="container h-100 d-flex align-items-center">
            <h1 class="text-white">{{ $activity->title }}</h1>
        </div>
    </div>
    <div id="description">
        <div class="container py-5">
            <div class="row mb-5">
                <div class="col-md-6 d-flex flex-column justify-content-center">
                    <h3>{{ $activity->subtitle_details }}</h3>
                    <p>{{ $activity->description }}</p>
                </div>
                <div class="col-md-6">
                    <div class="row justify-content-center">
                        @foreach ($images as $image)
                            <div class="col-md-6 p-2">
                                <img src="{{$image}}" class="img-fluid rounded shadow-sm" alt="">
                            </div>
                        @endforeach
                      </div>
                </div>
            </div>
        </div>
    </div>
    <div id="advantages" class="bg-white py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{ asset('img/advice.jpg') }}" class="img-fluid" alt="">
                </div>
                <div class="col-md-6">
                <h3>Pas convaincu ? Voici encore {{sizeof($informations)}} bonnes raisons :</h3>
                    <div class="d-flex flex-column">
                        @foreach ($informations as $information)
                            <div class="d-flex align-items-center info my-3">
                                <div class="icon position-relative">
                                    {!! $information->icon !!}
                                </div>
                                <div class="ml-3">
                                    <h5 class="font-weight-bold">{{$information->title}}</h5>
                                    <p class="m-0">{{$information->subtitle}}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
